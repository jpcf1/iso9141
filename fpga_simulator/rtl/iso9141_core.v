module iso9141_core
	(input clk_sys,
	 input clk_5,
	 input clk_10K4,
	 input reset,
	 input new_rx_data,
	 input [7:0] rx_byte,
	 input reg_value,
	 output reg [10:0] reg_addr,
	 output reg new_tx_data,
	 output reg [7:0] tx_byte);

