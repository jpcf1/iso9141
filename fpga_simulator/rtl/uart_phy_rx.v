module uart_phy_rx
   (input uart_rx_pin,
	input clk_baudX8,
	input reset,
	input enable,
	output reg dataStrobe,
	output reg [7:0] recv_byte);


   reg uart_rx, uart_rx_delay, sample_bit;
   reg [3:0] count, count_recv_bit, count_next, count_recv_bit_next;
   wire fall_edge_det;
   reg [3:0] STATE, STATE_NEXT;

   // The state tags
   localparam IDLE 			       = 4'd0;
   localparam CONF_START_BIT       = 4'd1;
   localparam GUARD_TIME_START_BIT = 4'd2;
   localparam SAMPLE_BIT           = 4'd3;
   localparam WAIT_SAMPLE_BIT      = 4'd4;
   localparam GUARD_TIME_STOP_BIT  = 4'd5;
   localparam CONF_STOP_BIT        = 4'd6;
   localparam RECV_BYTE_SUCCESS    = 4'd7;
   
   // Sync the incoming data to the oversampled baud clock
   always @(posedge clk_baudX8) begin
   		if(enable) begin
   			uart_rx 	  <= uart_rx_pin;
   			uart_rx_delay <= uart_rx;
   		end
   		else begin
   			uart_rx       <= 1;
   			uart_rx_delay <= 1;
   		end
   end

   // Negedge detection
   assign fall_edge_det = uart_rx ^ uart_rx_delay;

   // Deserializing data
   always @(posedge clk_baudX8) begin
   		if(sample_bit) begin
   			recv_byte <= {recv_byte[6:0], uart_rx};
   		end
   		else if(reset) begin
   			recv_byte <= 8'd0;
   		end
   		else begin
   			recv_byte <= recv_byte;
   		end
   end


   // -------------------------- The FSM
   always @(posedge clk_baudX8) begin
   		if (reset) begin
   			STATE <= IDLE;
   			count <= 4'd0;
   			count_recv_bit <= 4'd0;
   		end
   		else begin
   			STATE <= STATE_NEXT;
   			count <= count_next;
   			count_recv_bit <= count_recv_bit_next;
   		end
   end

   // Next state evaluation
   always @(*) begin
   		case(STATE)
   			IDLE: 
   			begin
   				if(fall_edge_det)
   					STATE_NEXT = CONF_START_BIT;
   				else
   					STATE_NEXT = IDLE;
   			end

   			CONF_START_BIT:
   			begin
   				if(count == 4'd6)
   					STATE_NEXT = GUARD_TIME_START_BIT;
   				else if(uart_rx)
   					STATE_NEXT = IDLE;
   				else
   				 	STATE_NEXT = CONF_START_BIT;
   			end

   			GUARD_TIME_START_BIT:
   			begin
   				if(count == 4'd3)
   					STATE_NEXT = SAMPLE_BIT;
   				else
   					STATE_NEXT = GUARD_TIME_START_BIT;
   			end

   			SAMPLE_BIT:
   			begin
   				if(count_recv_bit == 4'd7)
   					STATE_NEXT = GUARD_TIME_STOP_BIT;
   				else begin
   					STATE_NEXT = WAIT_SAMPLE_BIT;
   				end
   			end

   			WAIT_SAMPLE_BIT:
   			begin
   				if(count == 4'd6)
   					STATE_NEXT = SAMPLE_BIT;
   				else
   					STATE_NEXT = WAIT_SAMPLE_BIT;
   			end

   			GUARD_TIME_STOP_BIT:
   			begin
   				if(count == 4'd4)
   					STATE_NEXT = CONF_STOP_BIT;
   				else begin
   				 	STATE_NEXT = GUARD_TIME_STOP_BIT;
   				end 
   			end

   			CONF_STOP_BIT:
   			begin
				if(count == 4'd6)
   					STATE_NEXT = RECV_BYTE_SUCCESS;
   				else if(!uart_rx)
   					STATE_NEXT = IDLE;
   				else
   				 	STATE_NEXT = CONF_STOP_BIT;
   			end

   			RECV_BYTE_SUCCESS:
   			begin
   				STATE_NEXT = IDLE;
   			end

   		endcase
   end

   // Control signals evaluation
   always @(*) begin
   		case(STATE)
   			IDLE: 
   			begin
	   			count_recv_bit_next = 4'd0;
	   			count_next = 4'd0;
	   			sample_bit = 0;
	   			dataStrobe = 0;
   			end

   			CONF_START_BIT:
   			begin
	   			count_recv_bit_next = 4'd0;
	   			if(count == 4'd6)
	   				count_next = 4'd0;
	   			else begin
	   				if(!uart_rx)
		   				count_next = count + 4'd1;
		   			else begin
		   				count_next = 4'd0;
		   			end
		   		end
	   			sample_bit = 0;
	   			dataStrobe = 0;
   			end

   			GUARD_TIME_START_BIT:
   			begin
	   			count_recv_bit_next = 4'd0;
	   			if(count == 4'd3)
	   				count_next = 4'd0;
	   			else
	   				count_next = count + 4'd1;
	   			sample_bit = 0;
	   			dataStrobe = 0;;
   			end

   			SAMPLE_BIT:
   			begin
	   			if(count_recv_bit == 4'd7)
	   				count_recv_bit_next = 4'd0;
	   			else
	   				count_recv_bit_next = count_recv_bit + 4'd1;
	   			count_next = 4'd0;
	   			sample_bit = 1;
	   			dataStrobe = 0;
   			end

   			WAIT_SAMPLE_BIT:
   			begin
	   			if(count == 4'd6)
	   				count_next = 4'd0;
	   			else
	   				count_next = count + 4'd1;
	   			count_recv_bit_next = count_recv_bit;
	   			sample_bit = 0;
	   			dataStrobe = 0;
   			end

   			GUARD_TIME_STOP_BIT:
   			begin
   				if(count == 4'd7)
	   				count_next = 4'd0;
	   			else
	   				count_next = count + 4'd1;
	   			count_recv_bit_next = 4'd0;
	   			sample_bit = 0;
	   			dataStrobe = 0; 
   			end

			CONF_STOP_BIT:
   			begin
	   			if(count == 4'd6)
	   				count_next = 4'd0;
	   			else begin
	   				if(uart_rx)
		   				count_next = count + 4'd1;
		   			else begin
		   				count_next = 4'd0;
		   			end
		   		end
	   			count_recv_bit_next = 4'd0;
	   			sample_bit = 0;
	   			dataStrobe = 0; 
   			end

   			RECV_BYTE_SUCCESS:
   			begin
	   			count_next = 4'd0;
	   			count_recv_bit_next = 4'd0;
	   			sample_bit = 0;
	   			dataStrobe = 1; 
   			end

   		endcase
   end






	

endmodule

