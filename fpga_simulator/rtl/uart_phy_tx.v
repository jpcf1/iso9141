module uart_phy_tx
  (input [7:0] trans_byte,
   input new_data, 
	input clk_baud,
	input reset,
	input enable,
   output reg uart_tx_pin,
   output reg tx_done);


   reg [7:0] tx_buffer;
   reg [3:0] count_trans_bit, count_trans_bit_next;
   reg sample_data, trans_bit;
   reg [1:0] STATE, STATE_NEXT;

   // The state tags
   localparam IDLE 			        = 3'd0;
   localparam SEND_START_BIT       = 3'd1;
   localparam SEND_BYTE            = 3'd2;
   localparam SEND_STOP_BIT        = 3'd3;
  
   // The tx_buffer SR
   always @(posedge clk_baud) begin
      if(reset)
         tx_buffer <= 8'd0;
      else if(sample_data)
         tx_buffer <= trans_byte;
      else if(trans_bit)
         tx_buffer <= {tx_buffer[6:0], tx_buffer[7]};
      else
         tx_buffer <= tx_buffer;
   end

   // Controlling the output pin
   always @(posedge clk_baud) begin
      if(reset)
         uart_tx_pin <= 1'b1;
      else if(trans_bit)
         uart_tx_pin <= tx_buffer[7];
      else
         uart_tx_pin <= 1'b1;
   end

   // -------------------------- The FSM
   always @(posedge clk_baud) begin
		if (reset) begin
			STATE <= IDLE;
			count_trans_bit <= 4'd0;
		end
		else begin
			STATE <= STATE_NEXT;
			count_trans_bit <= count_trans_bit_next;
		end
   end

   // Next state evaluation
   always @(*) begin
   		case(STATE)
   			IDLE: 
   			begin
   				if(new_data)
   					STATE_NEXT = SEND_START_BIT;
   				else
   					STATE_NEXT = IDLE;
   			end

   			SEND_START_BIT:
   			begin
   				STATE_NEXT = SEND_BYTE;
   			end

   			SEND_BYTE:
   			begin
               if(count_trans_bit == 4'd7)
   				  STATE_NEXT = SEND_STOP_BIT;
   			end

   			SEND_STOP_BIT:
   			begin
               if(new_data)
   				  STATE_NEXT = SEND_START_BIT;
               else
                 STATE_NEXT = IDLE;
   			end
   		endcase
   end

   // outputs evaluation
   always @(*) begin
         case(STATE)
            IDLE: 
            begin
               uart_tx_pin = 1'b1;
               tx_done     = 1'b0;
               count_trans_bit_next = 4'd0;
               sample_data = 1'b0;
               trans_bit   = 1'b0;              
            end

            SEND_START_BIT:
            begin
               uart_tx_pin = 1'b0;
               tx_done     = 1'b0;
               count_trans_bit_next = 4'd0;
               sample_data = 1'b1;
               trans_bit   = 1'b0;
            end

            SEND_BYTE:
            begin
               uart_tx_pin = tx_buffer[7];
               tx_done     = 1'b0;
               if(count_trans_bit == 4'd7)
                  count_trans_bit_next = 4'd0;
               else
                  count_trans_bit_next = count_trans_bit + 4'd1;
               sample_data = 1'b0;
               trans_bit   = 1'b1;
            end

            SEND_STOP_BIT:
            begin
               uart_tx_pin = 1'b1;
               tx_done     = 1'b1;
               count_trans_bit_next = 4'd0;
               sample_data = 1'b0;
               trans_bit   = 1'b0;
            end
         endcase
   end
endmodule

