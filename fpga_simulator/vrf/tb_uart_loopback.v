`timescale 1ns/1ns

module tb_uart_loopback();
   
	parameter BIT_TIME   = 8000;
	parameter BIT_TIMEX8 = BIT_TIME/8; 

	reg clk, clkX8, reset, enable, new_data;
	wire dataStrobe, uart_wire, tx_done;
	wire [7:0] recv_byte;
	reg [7:0] trans_byte;

	uart_phy_tx UART_PHY_TX 
					(.trans_byte(trans_byte),
   					 .new_data(new_data),
					 .clk_baud(clk),
					 .reset(reset),
					 .enable(enable),
    				 .uart_tx_pin(uart_wire),
   					 .tx_done(tx_done));

	uart_phy_rx UART_PHY_RX 
					(.uart_rx_pin(uart_wire),
					.clk_baudX8(clkX8),
					.reset(reset),
					.enable(enable),
					.dataStrobe(dataStrobe),
					.recv_byte(recv_byte));


	always begin
		#(BIT_TIMEX8/2) clkX8 = ~clkX8;
	end

	always begin
		#(BIT_TIME/2) clk = ~clk;
	end

	initial begin
		$dumpfile("work/wave.vcd");
    	$dumpvars(0, tb_uart_loopback);

		clkX8  = 0;
		clk    = 0;
		reset  = 0;
		enable = 1;
		new_data = 1;

		#(BIT_TIMEX8*2)
		reset = 1;
		#(BIT_TIMEX8*5)
		reset = 0;

		#(BIT_TIMEX8*2)

		@(posedge clk)
		trans_byte = $random;

		repeat(50) begin
			@(posedge tx_done)
			trans_byte = $random;
		end

		#(BIT_TIME*20);

		$finish;

	end
endmodule 
