`timescale 1ns/1ns

module tb_uart_rx_phy_quick();
   
	parameter BIT_TIME   = 8000;
	parameter BIT_TIMEX8 = BIT_TIME/8; 

	reg rx, clkX8, reset, enable;
	wire dataStrobe;
	wire [7:0] recv_byte;
	reg [7:0] send_byte;

	uart_phy_rx UART_PHY_RX (.uart_rx_pin(rx),
							.clk_baudX8(clkX8),
							.reset(reset),
							.enable(enable),
							.dataStrobe(dataStrobe),
							.recv_byte(recv_byte));


	always begin
		#(BIT_TIMEX8/2) clkX8 = ~clkX8;
	end

	// Checks if received data matches
	always @(posedge dataStrobe) begin
		#(1)
		if(send_byte != recv_byte)
			$display("ERROR: wrong data");
		else
			$display("GOOD:  match data");
	end

	initial begin
		$dumpfile("work/wave.vcd");
    	$dumpvars(0, tb_uart_rx_phy_quick);

		clkX8  = 0;
		rx     = 1;
		reset  = 0;
		enable = 1;

		#(BIT_TIMEX8*2)
		reset = 1;
		#(BIT_TIMEX8*5)
		reset = 0;

		#(BIT_TIMEX8*2)

		repeat(50) begin
			// Start bit
			send_byte = $random;
			rx = 0;
			#(BIT_TIME);

			// 8bit word
			rx = send_byte[7];
			#(BIT_TIME);
			rx = send_byte[6];
			#(BIT_TIME);
			rx = send_byte[5];
			#(BIT_TIME);
			rx = send_byte[4];
			#(BIT_TIME);
			rx = send_byte[3];
			#(BIT_TIME);
			rx = send_byte[2];
			#(BIT_TIME);
			rx = send_byte[1];
			#(BIT_TIME);
			rx = send_byte[0];
			#(BIT_TIME);

			// Stop bit
			rx = 1;
			#(BIT_TIME);
			
		end

		#(BIT_TIME*20);

		$finish;

	end
endmodule 
