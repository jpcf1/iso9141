`timescale 1ns/1ns

module tb_uart_tx_phy_quick();
   
	parameter BIT_TIME   = 8000;

	reg clk, reset, enable, new_data;
	wire tx, tx_done;
	reg [7:0] send_byte;

	uart_phy_tx UART_PHY_TX 
					(.trans_byte(send_byte),
   					 .new_data(new_data),
					 .clk_baud(clk),
					 .reset(reset),
					 .enable(enable),
    				 .uart_tx_pin(tx),
   					 .tx_done(tx_done));

	always begin
		#(BIT_TIME/2) clk = ~clk;
	end

	initial begin
		$dumpfile("work/wave.vcd");
    	$dumpvars(0, tb_uart_tx_phy_quick);

		clk  = 0;
		reset  = 0;
		enable = 1;
		new_data = 0;

		#(BIT_TIME*2)
		reset = 1;
		#(BIT_TIME*5)
		reset = 0;

		#(BIT_TIME*2)

		new_data = 1;

		repeat(50) begin
			// Start bit
			send_byte = $random;
			
			@(tx_done);
			
		end
		new_data = 0;
		
		#(BIT_TIME*20);

		$finish;

	end
endmodule 
